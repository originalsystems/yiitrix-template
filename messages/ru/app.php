<?php

return [
    'Invalid login or password.' => 'Неверный логин или пароль.',
    'Login' => 'Логин',
    'Main page' => 'Главная',
    'Password' => 'Пароль',
    'Remember me' => 'Запомнить меня',
    'Sign in' => 'Вход',
    'Sign out' => 'Выход',
    'Submit' => 'Отправить',
];
