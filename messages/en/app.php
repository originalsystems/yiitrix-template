<?php

return [
    'Invalid login or password.' => 'Invalid login or password.',
    'Login' => 'Login',
    'Main page' => 'Main page',
    'Password' => 'Password',
    'Remember me' => 'Remember me',
    'Sign in' => 'Sign in',
    'Sign out' => 'Sign out',
    'Submit' => 'Submit',
];
