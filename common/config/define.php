<?php

declare(strict_types=1);

if (file_exists(__DIR__ . '/define.local.php')) {
    require __DIR__ . '/define.local.php';
}

defined('PHP_BIN') or define('PHP_BIN', '/usr/bin/php');

define('APP_ROOT', dirname(dirname(__DIR__)));
define('WEB_ROOT', APP_ROOT . '/frontend/web');

defined('DIR_MODE') or define('DIR_MODE', 02775);
defined('FILE_MODE') or define('FILE_MODE', 0664);

defined('YII_DEBUG') or define('YII_DEBUG', false);
defined('YII_ENV') or define('YII_ENV', 'prod');
