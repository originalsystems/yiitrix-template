<?php

declare(strict_types=1);

return [
    'components' => [
        'db' => [
            'dsn'      => 'pgsql:host=localhost;dbname=dbname',
            'username' => 'username',
            'password' => 'password',
        ],
    ],
];
