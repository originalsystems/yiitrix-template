<?php

declare(strict_types=1);

defined('DIR_MODE') or define('DIR_MODE', 02775);
defined('FILE_MODE') or define('FILE_MODE', 0664);

define('YII_DEBUG', false);
define('YII_ENV', 'prod');
