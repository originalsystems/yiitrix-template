<?php

declare(strict_types=1);

use yii\db\Connection;
use yii\log\FileTarget;

return \yii\helpers\ArrayHelper::merge([
    'id'             => 'yiitrix-template',
    'basePath'       => dirname(dirname(__DIR__)),
    'components'     => [
        'authManager' => [
            'class' => \common\rbac\AuthManager::class,
            'cache' => 'cache',
        ],
        'cache'       => [
            'class'     => \yii\redis\Cache::class,
            'redis'     => 'redis',
            'keyPrefix' => 'cache:',
        ],
        'db'          => [
            'class'               => Connection::class,
            'dsn'                 => 'pgsql:host=localhost;dbname=dbname',
            'username'            => 'username',
            'password'            => 'password',
            'enableSchemaCache'   => true,
            'schemaCacheDuration' => 3600,
        ],
        'i18n'        => [
            'class'        => \yii\i18n\I18N::class,
            'translations' => [
                'yii2kernel' => [
                    'class'          => \yii\i18n\PhpMessageSource::class,
                    'basePath'       => '@yii2kernel/messages',
                    'sourceLanguage' => 'en-US',
                ],
                'yiitrix'    => [
                    'class'          => \yii\i18n\PhpMessageSource::class,
                    'basePath'       => '@yiitrix/messages',
                    'sourceLanguage' => 'en-US',
                ],
                'app'        => [
                    'class'          => \yii\i18n\PhpMessageSource::class,
                    'basePath'       => '@app/messages',
                    'sourceLanguage' => 'en-US',
                ],
            ],
        ],
        'redis'       => [
            'class'    => \yii\redis\Connection::class,
            'port'     => 6379,
            'database' => 0,
        ],
    ],
    'language'       => 'ru',
    'languages'      => ['ru', 'en'],
    'name'           => 'Yii2 shop template',
    'params'         => [],
    'runtimePath'    => '@common/runtime',
    'sourceLanguage' => 'en',
], require __DIR__ . '/main.local.php');
