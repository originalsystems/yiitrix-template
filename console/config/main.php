<?php

declare(strict_types=1);

use yii\log\FileTarget;

return \yii\helpers\ArrayHelper::merge(require APP_ROOT . '/common/config/main.php', [
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'console\controllers',
    'controllerMap'       => [
        'message' => [
            'class'            => \yii\console\controllers\MessageController::class,
            'sourcePath'       => '@app',
            'messagePath'      => '@app/messages',
            'languages'        => [
                'en',
                'ru',
            ],
            'translator'       => 'Yii::t',
            'sort'             => true,
            'overwrite'        => true,
            'removeUnused'     => true,
            'markUnused'       => false,
            'except'           => [
                '.git',
                '.gitignore',
                '.gitkeep',
                '/common/runtime',
                '/frontend/web',
                '/messages',
                '/node_modules',
                '/vendor',
            ],
            'only'             => [
                '*.php',
            ],
            'format'           => 'php',
            'catalog'          => 'messages',
            'ignoreCategories' => [
                'yii',
                'yiitrix',
            ],
            'phpFileHeader'    => '',
            'phpDocBlock'      => false,
        ],
        'migrate' => [
            'class'         => \yii\console\controllers\MigrateController::class,
            'templateFile'  => '@yii2kernel/console/views/migration.php',
            'migrationPath' => [
                '@console/migrations',
                '@yiitrix/console/migrations',
            ],
        ],
        'upload'  => [
            'class' => \yiitrix\console\controllers\UploadController::class,
        ],
    ],
    'components'          => [
        'log'        => [
            'targets' => [
                [
                    'class'   => FileTarget::class,
                    'levels'  => ['error', 'warning'],
                    'logFile' => '@runtime/logs/console.log',
                ],
            ],
        ],
        'urlManager' => [
            'class'               => \yii\web\UrlManager::class,
            'enablePrettyUrl'     => true,
            'enableStrictParsing' => false,
            'normalizer'          => [
                'class'                  => \yii\web\UrlNormalizer::class,
                'collapseSlashes'        => true,
                'normalizeTrailingSlash' => true,
            ],
            'rules'               => require APP_ROOT . '/frontend/config/rules.php',
            'showScriptName'      => false,
            'suffix'              => '/',
        ],
    ],
], require __DIR__ . '/main.local.php');
