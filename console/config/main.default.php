<?php

declare(strict_types=1);

return [
    'components' => [
        'urlManager' => [
            'baseUrl' => 'http://site.local',
        ],
    ],
];
