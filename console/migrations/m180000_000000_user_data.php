<?php

declare(strict_types=1);

use yii\db\Migration;
use yiitrix\models\User;

class m180000_000000_user_data extends Migration
{
    public function safeUp()
    {
        $security = \Yii::$app->getSecurity();
        $auth     = \Yii::$app->getAuthManager();

        $adminRole  = $auth->getRole(\common\rbac\AuthManager::ROLE_ADMIN);
        $clientRole = $auth->getRole(\common\rbac\AuthManager::ROLE_CLIENT);

        $admin = new User();

        $admin->status        = User::STATUS_ACTIVE;
        $admin->login         = 'admin';
        $admin->email         = 'admin@application.com';
        $admin->auth_key      = $security->generateRandomString(32);
        $admin->password_hash = $security->generatePasswordHash('admin-password');
        $admin->access_token  = $security->generateRandomString(16);

        $admin->strictSave();

        $client = new User();

        $client->status        = User::STATUS_ACTIVE;
        $client->login         = 'client';
        $client->email         = 'client@application.com';
        $client->auth_key      = $security->generateRandomString(32);
        $client->password_hash = $security->generatePasswordHash('client-password');
        $client->access_token  = $security->generateRandomString(16);

        $client->strictSave();

        $auth->assign($adminRole, $admin->id);
        $auth->assign($clientRole, $client->id);
    }

    public function safeDown()
    {
        User::deleteAll([
            'login' => [
                'admin',
                'client',
            ],
        ]);
    }
}
