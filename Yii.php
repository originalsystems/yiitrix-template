<?php

declare(strict_types=1);

use yii\di\Container;

class Yii extends \yii\BaseYii
{
    public static $aliases = [
        '@yii'          => APP_ROOT . '/vendor/yiisoft/yii2',
        '@yii2kernel'   => APP_ROOT . '/vendor/originalsystems/yii2-app-kernel',
        '@yiitrix'      => APP_ROOT . '/vendor/originalsystems/yiitrix-kernel',
        '@common'       => APP_ROOT . '/common',
        '@console'      => APP_ROOT . '/console',
        '@frontend'     => APP_ROOT . '/frontend',
        '@node_modules' => APP_ROOT . '/node_modules',
        '@upload'       => APP_ROOT . '/frontend/web/upload',
    ];

    /**
     * @var \yiitrix\web\Application|\yiitrix\console\Application
     */
    public static $app;

    /**
     * @var \yiitrix\models\User
     */
    public static $user;
}

spl_autoload_register(['Yii', 'autoload'], true, true);

$coreMap   = require APP_ROOT . '/vendor/yiisoft/yii2/classes.php';
$extendMap = require APP_ROOT . '/vendor/originalsystems/yii2-app-kernel/classes.php';
$kernelMap = require APP_ROOT . '/vendor/originalsystems/yiitrix-kernel/classes.php';

\Yii::$classMap  = array_merge($coreMap, $extendMap, $kernelMap);
\Yii::$container = new Container();
