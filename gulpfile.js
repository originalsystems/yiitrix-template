'use strict';

const gulp       = require('gulp'),
      sass       = require('gulp-sass'),
      csso       = require('gulp-csso'),
      sourcemaps = require('gulp-sourcemaps'),
      merge      = require('merge-stream'),
      assets     = [
          'common/assets',
          'frontend/assets'
      ];

gulp.task('scss', () => {
    return merge(assets.map(dir => {
        return gulp.src(dir + '/**/*.scss')
            .pipe(sourcemaps.init())
            .pipe(sass().on('error', sass.logError))
            .pipe(csso({restructure: false}))
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest(dir));
    }));
});

gulp.task('scss:watch', () => {
    for (const dir of assets) {
        gulp.watch(dir + '/**/*.scss', ['scss']);
    }
});

gulp.task('default', ['scss:watch']);
