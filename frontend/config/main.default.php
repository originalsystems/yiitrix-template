<?php

declare(strict_types=1);

$config = [
    'components' => [
        'request' => [
            'cookieValidationKey' => '',
        ],
    ],
];

if (YII_ENV_PROD === false) {
    $config['bootstrap'][] = 'debug';
    $config['bootstrap'][] = 'gii';

    $config['modules']['debug'] = [
        'class'      => \yii\debug\Module::class,
        'allowedIPs' => ['192.168.*', '127.*'],
        'dirMode'    => DIR_MODE,
    ];
    $config['modules']['gii']   = [
        'class'       => \yii\gii\Module::class,
        'allowedIPs'  => ['192.168.*', '127.*'],
        'newDirMode'  => DIR_MODE,
        'newFileMode' => FILE_MODE,
        'generators'  => [
            'model' => [
                'class' => \yii2kernel\gii\generators\model\Generator::class,
            ],
        ],
    ];
}

return $config;
