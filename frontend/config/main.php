<?php

declare(strict_types=1);

use yii\log\FileTarget;
use yii\web\Session;
use yii2kernel\web\AssetManager;
use yii2kernel\web\Request;
use yii2kernel\web\Response;
use yii2kernel\web\View;

return \yii\helpers\ArrayHelper::merge(require APP_ROOT . '/common/config/main.php', [
    'bootstrap'           => ['admin', 'log', 'user'],
    'controllerNamespace' => 'frontend\controllers',
    'components'          => [
        'assetManager' => [
            'class'           => AssetManager::class,
            'appendTimestamp' => true,
            'compressCss'     => YII_ENV_PROD,
            'compressJs'      => YII_ENV_PROD,
            'dirMode'         => DIR_MODE,
            'fileMode'        => FILE_MODE,
            'linkAssets'      => true,
        ],
        'errorHandler' => [
            'class'       => \yii2kernel\web\ErrorHandler::class,
            'errorAction' => 'site/error',
        ],
        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'   => FileTarget::class,
                    'levels'  => ['error', 'warning'],
                    'logFile' => '@runtime/logs/frontend.log',
                    'logVars' => ['_GET', '_POST', '_FILES', '_SERVER'],
                ],
            ],
        ],
        'response'     => [
            'class' => Response::class,
        ],
        'request'      => [
            'class' => Request::class,
        ],
        'session'      => [
            'class' => Session::class,
        ],
        'view'         => [
            'class' => View::class,
        ],
        'urlManager'   => [
            'class'               => \yii\web\UrlManager::class,
            'enablePrettyUrl'     => true,
            'enableStrictParsing' => false,
            'normalizer'          => [
                'class'                  => \yii\web\UrlNormalizer::class,
                'collapseSlashes'        => true,
                'normalizeTrailingSlash' => true,
            ],
            'rules'               => require __DIR__ . '/rules.php',
            'showScriptName'      => false,
            'suffix'              => '/',
        ],
        'user'         => [
            'class'           => \yiitrix\web\User::class,
            'identityClass'   => \yiitrix\models\User::class,
            'loginUrl'        => ['/auth/login'],
            'enableAutoLogin' => true,
        ],
    ],
    'modules'             => [
        'admin' => [
            'class' => \yiitrix\modules\admin\Module::class,
        ],
    ],
    'viewPath'            => '@frontend/views',
], require __DIR__ . '/main.local.php');
