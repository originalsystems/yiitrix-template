<?php

declare(strict_types=1);

$rawRules = [
    '<controller:page>/<view:[\w-]+>' => '<controller>/view',

    ''                                    => 'site/index',
    '<controller:[\w-]+>'                 => '<controller>',
    '<controller:[\w-]+>/<action:[\w-]+>' => '<controller>/<action>',
];

$rules = [];

foreach ($rawRules as $rule => $route) {
    $rules["<_lang:(\w\w)>/{$rule}"] = $route;
    $rules[$rule]                    = $route;
}

return $rules;
