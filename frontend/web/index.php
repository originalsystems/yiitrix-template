<?php

declare(strict_types=1);

require dirname(dirname(__DIR__)) . '/common/config/define.php';

require APP_ROOT . '/vendor/autoload.php';
require APP_ROOT . '/Yii.php';
require APP_ROOT . '/common/config/bootstrap.php';

$exitCode = (new \yiitrix\web\Application(require APP_ROOT . '/frontend/config/main.php'))->run();

exit($exitCode);
