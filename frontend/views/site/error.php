<?php

/**
 * @var \yii2kernel\web\View $this
 * @var string               $name
 * @var string               $message
 * @var \yii\base\Exception  $exception
 */

declare(strict_types=1);

?>

<section class="section-site-error">
    <div class="container">
        <div class="title"><?= $name; ?></div>
        <div class="description"><?= $message; ?></div>
    </div>
</section>
