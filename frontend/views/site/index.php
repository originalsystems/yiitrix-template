<?php

/**
 * @var \yii2kernel\web\View $this
 */

declare(strict_types=1);
?>
<section class="section-site-index">
    <div class="container">
        <div class="jumbotron">
            <h1><?= Yii::$app->name; ?></h1>
        </div>
    </div>
</section>
