<?php

/**
 * @var \yii2kernel\web\View $this
 * @var string               $content
 */

declare(strict_types=1);

use yii\helpers\Html;

\frontend\assets\LayoutAsset::register($this);

?>

<?php $this->beginPage(); ?>

<!DOCTYPE html>
<html lang="<?= \Yii::$app->language; ?>">
<head>
    <meta charset="<?= \Yii::$app->charset; ?>">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title ? : \Yii::$app->name); ?></title>
    <?php $this->head(); ?>
</head>
<body>

<?php $this->beginBody(); ?>

<header>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button"
                        class="navbar-toggle collapsed"
                        data-toggle="collapse"
                        data-target="#header-navbar"
                        aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brand" href="/"><?= Yii::$app->name; ?></a>
            </div>

            <div class="collapse navbar-collapse" id="header-navbar">
                <?= \yii\bootstrap\Nav::widget([
                    'items'   => [
                        [
                            'url'   => ['/site/index'],
                            'label' => Yii::t('app', 'Main page'),
                        ],
                        [
                            'url'     => ['/auth/login'],
                            'label'   => Yii::t('app', 'Sign in'),
                            'visible' => Yii::$app->getUser()->getIsGuest(),
                        ],
                        [
                            'url'     => ['/auth/logout'],
                            'label'   => Yii::t('app', 'Sign out'),
                            'visible' => Yii::$app->getUser()->getIsGuest() === false,
                        ],
                    ],
                    'options' => [
                        'class' => 'nav navbar-nav',
                    ],
                ]); ?>
            </div>
        </div>
    </nav>
</header>

<div class="page-wrapper">
    <?= $content; ?>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y'); ?></p>

        <p class="pull-right"><?= \Yii::powered(); ?></p>
    </div>
</footer>

<?php $this->endBody(); ?>

</body>
</html>

<?php $this->endPage(); ?>
