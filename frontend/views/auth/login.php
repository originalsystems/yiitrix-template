<?php

/**
 * @var \yii2kernel\web\View  $this
 * @var \frontend\forms\Login $formModel
 */

declare(strict_types=1);

use yii\helpers\Html;

?>

<section class="section-auth-login">
    <div class="container">
        <?= Html::beginForm(); ?>

        <div class="form-group">
            <label for="login-input"><?= Yii::t('app', 'Login'); ?></label>

            <?= Html::activeTextInput($formModel, 'login', [
                'id'    => 'login-input',
                'class' => 'form-control',
            ]); ?>
        </div>

        <div class="form-group">
            <label for="password-input"><?= Yii::t('app', 'Password'); ?></label>

            <?= Html::activePasswordInput($formModel, 'password', [
                'id'    => 'password-input',
                'class' => 'form-control',
            ]); ?>
        </div>

        <div class="checkbox">
            <?= Html::activeCheckbox($formModel, 'remember_me'); ?>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">
                <?= Yii::t('app', 'Submit'); ?>
            </button>
        </div>

        <?= Html::endForm(); ?>
    </div>
</section>
