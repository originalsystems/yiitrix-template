<?php

declare(strict_types=1);

namespace frontend\forms;

use yii2kernel\base\FormModel;
use yiitrix\models\User;

class Login extends FormModel
{
    public $login;
    public $password;
    public $remember_me = true;

    public function rules(): array
    {
        return [
            [['login', 'password'], 'required'],
            [['remember_me'], 'boolean'],
            [['password'], 'validatePassword'],
        ];
    }

    public static function labels(): array
    {
        return [
            'login'       => \Yii::t('app', 'Login'),
            'password'    => \Yii::t('app', 'Password'),
            'remember_me' => \Yii::t('app', 'Remember me'),
        ];
    }

    public function validatePassword($attribute, $params): void
    {
        if ($this->hasErrors()) {
            return;
        }

        $user = $this->getUser();

        if ($user === null) {
            $this->addError($attribute, \Yii::t('app', 'Invalid login or password.'));

            return;
        }

        try {
            $isValid = \Yii::$app->getSecurity()->validatePassword($this->password, $user->password_hash);
        } catch (\Exception $ex) {
            $isValid = false;
        }

        if ($isValid === false) {
            $this->addError($attribute, \Yii::t('app', 'Invalid login or password.'));

            return;
        }
    }

    /**
     * @var User
     */
    private $_user = false;

    /**
     * @return User|null
     */
    protected function getUser(): ?\yiitrix\models\User
    {
        if ($this->_user === false) {
            $this->_user = User::find()->where(['login' => $this->login, 'status' => User::STATUS_ACTIVE])->one();
        }

        return $this->_user;
    }

    public function save(): bool
    {
        if ($this->validate() === false) {
            return false;
        }

        return \Yii::$app->getUser()->login($this->getUser(), $this->remember_me ? 2592000 : 0);
    }
}
