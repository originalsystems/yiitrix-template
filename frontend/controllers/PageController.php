<?php

declare(strict_types=1);

namespace frontend\controllers;

use yii\web\ViewAction;
use yii2kernel\web\Controller;

class PageController extends Controller
{
    public function actions()
    {
        return [
            'view' => [
                'class'      => ViewAction::class,
                'viewParam'  => 'view',
                'viewPrefix' => null,
            ],
        ];
    }
}
