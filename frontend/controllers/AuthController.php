<?php

declare(strict_types=1);

namespace frontend\controllers;

use frontend\forms\Login;

class AuthController extends \yii2kernel\web\Controller
{
    /**
     * @return string|\yii\web\Response
     * @throws \yii\base\InvalidParamException
     */
    public function actionLogin()
    {
        $formModel = new Login();

        if ($formModel->load(\Yii::$app->getRequest()->post())) {
            $formModel->save();

            if (\Yii::$app->getRequest()->getIsAjax()) {
                return $this->json([], $formModel->getErrors());
            }

            if ($formModel->hasErrors() === false) {
                return $this->goBack();
            }
        }

        return $this->render('login', [
            'formModel' => $formModel,
        ]);
    }

    public function actionLogout()
    {
        \Yii::$app->getUser()->logout();

        if (\Yii::$app->getRequest()->getIsAjax()) {
            return $this->json();
        }

        return $this->goHome();
    }
}
