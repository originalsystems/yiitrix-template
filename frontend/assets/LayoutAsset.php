<?php

declare(strict_types=1);

namespace frontend\assets;

use yii\bootstrap\BootstrapPluginAsset;
use yii\bootstrap\BootstrapAsset;
use yii\web\YiiAsset;
use yii2kernel\assets\FileDependAsset;
use yii2kernel\assets\FontsAsset;
use yiitrix\web\assets\AppAsset;

class LayoutAsset extends FileDependAsset
{
    public $sourcePath = '@frontend/assets/layout';
    public $depends    = [
        YiiAsset::class,
        BootstrapAsset::class,
        BootstrapPluginAsset::class,
        AppAsset::class,
        FontsAsset::class,
    ];
}
